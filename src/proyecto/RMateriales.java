
package proyecto;

import java.awt.Frame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author max
 */
public class RMateriales {
    private Connection cn;
    public RMateriales(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn=DriverManager.getConnection("jdbc:mysql://localhost/industrial","root","");
            
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    public void mostrarReporte()throws Exception{        
        JasperReport report= JasperCompileManager.compileReport(System.getProperty("user.dir").concat("/reports/materiales.jrxml"));
        JasperPrint print= JasperFillManager.fillReport(report, null,cn);
        JasperViewer view=new JasperViewer(print,false);
        view.setTitle("Lista de Cursos");
        view.setExtendedState(Frame.MAXIMIZED_BOTH);
        view.setVisible(true);
    }
    
}

