
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Maquinaria {
    conectate con;
    
    public Maquinaria()
    {
        con =  new  conectate();
    }
    public void NuevaMaqunaria(String codpat, String nombre, String marca, String descripcion, String estado){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "maquinaria(codpat, nombre, marca, descripcion, estado)" +
                    " VALUES(?,?,?,?,?)");            
            pstm.setString(1, codpat);
            pstm.setString(2, nombre);
            pstm.setString(3, marca);
            pstm.setString(4, descripcion); 
            pstm.setString(5,estado);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
           JOptionPane.showMessageDialog(null, "La maquinaria  ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
    public void updateMaquinaria(String codpat, String nombre, String marca, String descripcion,String estado){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update maquinaria " +
            "set codpat = ? ," +
            "nombre = ? ," +
            "marca = ? ," +                    
            "descripcion = ?, " +
            "estado = ?"+
            "where codpat = ? ");            
            pstm.setString(1, codpat);                   
            pstm.setString(2, nombre);
            pstm.setString(3, marca);
            pstm.setString(4, descripcion);
            pstm.setString(5, estado);
            pstm.setString(6, String.valueOf(codpat));
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
 
   public void deleteMaquinaria(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from maquinaria where codpat = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM maquinaria ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][6];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            " codpat, nombre, marca, descripcion,  estado " +
            " FROM maquinaria" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            
            
            String num = "00"+Integer.toString(i+1);
            String estCodpat = res.getString("codpat");
            String estNombre = res.getString("nombre");
            String estMarca = res.getString("marca");
            String estdescripcion = res.getString("descripcion");
            String estEstado = res.getString("estado");
                        
            data[i][0] = num;
            data[i][1] = estCodpat;            
            data[i][2] = estNombre;    
            data[i][3] = estMarca;
            data[i][4] = estdescripcion;                                
            data[i][5]= estEstado;
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 } 
}
