package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author max
 */
public class Area {
    conectate con;
    public Area()
    {
        con =   new   conectate();
    }
/*Añade un nuevo registro*/
public void NuevoCurso(String codigo, String nombre, String descripcion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "curso(codigo,nombre,descripcion)" +
                    " VALUES(?,?,?)");            
            pstm.setString(1, codigo);
            pstm.setString(2, nombre);
            pstm.setString(3, descripcion);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
           JOptionPane.showMessageDialog(null, "Curso ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
 public void updateCurso(String codigo, String nombre, String descripcion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update curso " +
            "set codigo = ? ," +
            "nombre = ? ," +
            "descripcion = ? " +                                                 
            "where codigo = ? ");            
            pstm.setString(1, codigo);                   
            pstm.setString(2, nombre);
            pstm.setString(3, descripcion);            
            pstm.setString(4, String.valueOf(codigo));
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
         
      }
   }
   public void deleteCurso(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from curso where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
     int suma=0;
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM curso ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][4];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codigo, nombre, descripcion"+
            " FROM curso" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+Integer.toString(i+1);            
            String estCodigo = res.getString("codigo");
            String estNombre = res.getString("nombre");
            String estDescripcion = res.getString("descripcion");
           
            
            data[i][0] = estid;
            data[i][1] = estCodigo;            
            data[i][2] = estNombre;
            data[i][3] = estDescripcion;
            i++;
         }
         
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }    

}
