package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author max
 */
public class ReposicionMa {
conectate  con;
public ReposicionMa()
{
    con =   new   conectate();
}

    //metodo para  actualizar datos  de una herramienta
    public void updateMaterial(String codigo, String cantidad){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update material set cantidad = ? where codigo = ?");            
            pstm.setString(1, cantidad);                   
            pstm.setString(2, codigo);            
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
 public void deleteMaterial(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from material where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
 
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
     int suma=0;
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM material ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][7];  
    
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codigo, nombre, descripcion, medida, marca, cantidad" +
            " FROM material" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+Integer.toString(i+1);
            String estCodigo = res.getString("codigo");
            String estNombre = res.getString("nombre");
            String estDescripcion = res.getString("descripcion");
            String estMedida = res.getString("medida");
            String estMarca = res.getString("marca");
            String estCantidad=res.getString("cantidad");
           
            
            data[i][0] = estid;
            data[i][1] = estCodigo;            
            data[i][2] = estNombre;    
            data[i][3] = estDescripcion;
            data[i][4] = estMedida;                                
            data[i][5] = estMarca;
            data[i][6] = estCantidad;
            i++;
         }
         
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }

}
