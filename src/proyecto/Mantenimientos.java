
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Mantenimientos {
     conectate  con;
     public Mantenimientos()
     {
         con = new conectate();
     }
       /*Añade un nuevo registro*/
   public void NuevoMantenimiento(String codigo, String maquinaria, String mecanico, String tecnico){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "mantenimiento(codigo, maquinaria, mecanico, tecnico)" +
                    " VALUES(?,?,?,?)");            
            pstm.setString(1, codigo);
            pstm.setString(2, maquinaria);
            pstm.setString(3, mecanico);
            pstm.setString(4, tecnico);                                                           
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         //System.out.println(e);
           JOptionPane.showMessageDialog(null, "Esta  Operacion no se pudo realizar con exito");
      }
   }
    //metodo para  actualizar datos  de una herramienta
    public void updateManteniemiento(String codigo, String maquinaria, String mecanico, String tecnico){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update mantenimiento " +
            "set codigo = ? ," +
            "maquinaria = ? ," +
            "mecanico = ? ," +                    
            "tecnico = ? " +                    
            "where codigo = ? ");            
            pstm.setString(1, codigo);                   
            pstm.setString(2, maquinaria);
            pstm.setString(3, mecanico);
            pstm.setString(4, tecnico);
            pstm.setString(5, codigo);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
     public void updateMaterial(String codigo, String cantidad){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update material set cantidad = ? where codigo = ?");            
            pstm.setString(1, cantidad);                   
            pstm.setString(2, codigo);            
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
   public void deleteMantenimiento(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from mantenimiento where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM mantenimiento ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][5];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codigo, maquinaria, mecanico, tecnico" +
            " FROM mantenimiento" +
            " ORDER BY maquinaria ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+(i+1);
            String estCodigo = res.getString("codigo");
            String estMaquinaria = res.getString("maquinaria");
            String estMecanico = res.getString("mecanico");
            String estTecnico = res.getString("tecnico");
           
            
            data[i][0] = estid;            
            data[i][1] = estCodigo;            
            data[i][2] = estMaquinaria;    
            data[i][3] = estMecanico;
            data[i][4] = estTecnico;                                
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }  
}
