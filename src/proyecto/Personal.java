
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author max
 */
public class Personal {
    conectate  con;
    
    public Personal (){
    con = new conectate();
  } 
  
  /*Añade un nuevo registro*/
   public void NuevoAlumno(String nombre, String apellido, String codigo, String ciclo, String curso){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "Alumno(nombre, apellido, codigo, ciclo, curso)" +
                    " VALUES(?,?,?,?,?)");            
            pstm.setString(1, nombre);
            pstm.setString(2, apellido);
            pstm.setString(3, codigo);                        
            pstm.setString(4, ciclo);      
            pstm.setString(5, curso);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
           JOptionPane.showMessageDialog(null, "El codigo del alumno ya existe");
      }
   }
    
    public void updateAlumno(String nombre, String apellido, String codigo, String ciclo,String curso){
       try {            
           try (PreparedStatement pstm = con.getConnection().prepareStatement("update Alumno " +
                   "set nombre = ? ," +
                   "apellido = ? ," +
                   "codigo = ? ," +
                   "ciclo = ? ," +
                   "curso = ?"+
                   "where codigo = ? ")) {
               pstm.setString(1, nombre);
               pstm.setString(2, apellido);
               pstm.setString(3, codigo);
               pstm.setString(4, ciclo);
               pstm.setString(5, curso);
               pstm.setString(6, String.valueOf(codigo));
               pstm.execute();
           }            
         }catch(SQLException e){
         System.out.println(e);
         
      }
   }
     public void buscarAlumno(String nombre){
       try {
            //select id, nombre, apellido, codigo, ciclo from alumno where nombre like '%zand%'
            PreparedStatement pstm = con.getConnection().prepareStatement("select id, nombre, apellido, codigo, ciclo from alumno where nombre like '%"+nombre+"%'");
            //pstm.execute();
            ResultSet res = pstm.executeQuery();
            pstm.close();           
         }catch(SQLException e){
         System.out.println(e);
      }
   }
     
   public void deleteAlumno(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from Alumno where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM Alumno ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][6];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "nombre, apellido,codigo, ciclo,curso " +
            " FROM Alumno" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String num = "00"+Integer.toString(i+1);
            String estNombre = res.getString("nombre");
            String estApellido = res.getString("apellido");
            String estCodigo = res.getString("codigo");
            String estCiclo = res.getString("ciclo");
            String estCurso = res.getString("curso");
            
            data[i][0] = num;
            data[i][1] = estNombre;            
            data[i][2] = estApellido;            
            data[i][3] = estCodigo;            
            data[i][4] = estCiclo;         
            data[i][5] = estCurso;
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }    
}


