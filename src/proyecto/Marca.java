package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author max
 */
public class Marca {
    conectate con;
    public Marca()
    {
        con =   new   conectate();
    }
/*Añade un nuevo registro*/
public void NuevaMarca(String nombre){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "marca(nombre)" +
                    " VALUES(?)");            
            pstm.setString(1, nombre);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
           JOptionPane.showMessageDialog(null, "La  marca ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
 
   public void deleteMarca(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from marca where nombre = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
     int suma=0;
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM marca ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][2];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "nombre" +
            " FROM marca" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+Integer.toString(i+1);            
            String estNombre = res.getString("nombre");
           
            
            data[i][0] = estid;
            data[i][1] = estNombre;            
            i++;
         }
         
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }    

}
