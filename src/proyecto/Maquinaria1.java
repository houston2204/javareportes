
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Maquinaria1 {
    conectate con;
    
    public Maquinaria1()
    {
        con =  new  conectate();
    }
    public void NuevaMaqunaria(String codigo, String nombre, String tipo, String marca, String modelo, String motor, String potencia, String fabricacion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "maquinaria1(codigo, nombre, tipo, marca, modelo, motor, potencia, fabricacion)" +
                    " VALUES(?,?,?,?,?,?,?,?)");            
            pstm.setString(1, codigo);
            pstm.setString(2, nombre);
            pstm.setString(3, tipo);
            pstm.setString(4, marca); 
            pstm.setString(5, modelo);
            pstm.setString(6, motor);
            pstm.setString(7, potencia);
            pstm.setString(8, fabricacion);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
           JOptionPane.showMessageDialog(null, "La maquinaria  ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
    public void updateMaquinaria(String codigo, String nombre, String tipo, String marca, String modelo, String motor, String potencia, String fabricacion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update maquinaria1 " +
            "set codigo = ? ," +
            "nombre = ? ," +
            "tipo = ? ," +                    
            "marca = ?," +
            "modelo = ?,"+
            "motor = ?,"+        
            "potencia = ?,"+      
            "fabricacion = ?"+
            "where codigo = ? ");  
            pstm.setString(1, codigo);
            pstm.setString(2, nombre);
            pstm.setString(3, tipo);
            pstm.setString(4, marca); 
            pstm.setString(5, modelo);
            pstm.setString(6, motor);
            pstm.setString(7, potencia);
            pstm.setString(8, fabricacion);
            pstm.setString(9, codigo);
            pstm.execute();
            pstm.close();
         }catch(SQLException e){
         System.out.println(e);
      }
   }
 
   public void deleteMaquinaria(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from maquinaria1 where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM maquinaria1 ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][9];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
            PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codigo, nombre, tipo, marca, modelo, motor, potencia, fabricacion" +
            " FROM maquinaria1" +
            " ORDER BY tipo ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            
            
            String num = "00"+Integer.toString(i+1);
            String estCodigo      = res.getString("codigo");
            String estNombre      = res.getString("nombre");
            String estTipo        = res.getString("tipo");
            String estMarca       = res.getString("marca");
            String estModelo      = res.getString("modelo");
            String estMotor       = res.getString("motor");
            String estPotencial   = res.getString("potencia");
            String estFabricacion = res.getString("fabricacion");
                        
            data[i][0] = num;
            data[i][1] = estCodigo;            
            data[i][2] = estNombre;    
            data[i][3] = estTipo;
            data[i][4] = estMarca;                                
            data[i][5] = estModelo;
            data[i][6] = estMotor;
            data[i][7] = estPotencial;
            data[i][8] = estFabricacion;            
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 } 
}
