/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author max
 */
public class reposicion_m extends javax.swing.JInternalFrame {

    /**
     * Creates new form Materiales
     */
    public reposicion_m() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    public void habilitar()
    {
     txtCodigo.setEditable(true);
     txtCantidad.setEditable(true);
     txtCodigo.requestFocus();
    }
    public void deshabilitar()
    {
     txtCodigo.setEditable(false);
     txtCantidad.setEditable(false);
    }
    public  void limpiar()
    {
        txtCodigo.setText("");
        txtCantidad.setText("");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtCodigo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        tbnBuscar = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        txtRetirar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detalles de materiales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 14))); // NOI18N

        txtCodigo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Codigo");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Cantidad");

        txtCantidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(72, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(63, 63, 63))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(72, 72, 72)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(302, 302, 302))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/salir.png"))); // NOI18N
        btnCancelar.setText("Salir");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnNuevo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/new_32.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/eliminar.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnActualizar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/ico_32x32.png"))); // NOI18N
        btnActualizar.setText("Ingresar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 14))); // NOI18N

        tbnBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/share.png"))); // NOI18N
        tbnBuscar.setText("buscar");
        tbnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbnBuscarActionPerformed(evt);
            }
        });

        txtBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(118, 118, 118)
                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(tbnBuscar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tbnBuscar)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Lista de materiales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 14))); // NOI18N

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/pdf.png"))); // NOI18N
        jButton3.setText("Imprimir lista");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtRetirar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtRetirar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/ico_32x32.png"))); // NOI18N
        txtRetirar.setText("Retirar");
        txtRetirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRetirarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(19, 19, 19))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtRetirar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnActualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnEliminar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCancelar)
                        .addComponent(btnNuevo)
                        .addComponent(btnEliminar)
                        .addComponent(txtRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed

        if (fila > -1){
            String codigo = String.valueOf(tabla.getValueAt(fila, 1));                        
            m.deleteMaterial(codigo);
            updateTabla();
            fila=-1;
        }
        limpiar();       
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        conectate  con = new conectate();
        int cantidad;
        int cant;
        if(!txtBuscar.getText().equals(""))
        {
        String[] columNames = {"N°","Codigo","Nombre","Descripcion","Medida","Marca, Cantidad"};

        try {
            //select id, nombre, apellido, codigo, ciclo from alumno where nombre like '%zand%'
            PreparedStatement pstm = con.getConnection().prepareStatement("SELECT codigo, nombre, descripcion, medida, marca, cantidad FROM material where codigo like '%"+txtBuscar.getText()+"%'");
            //SELECT codigo, nombre, descripcion, medida, marca, cantidad FROM material where codigo like '%"+txtCodigo.getText()+"%'
            pstm.execute();
            ResultSet res = pstm.executeQuery();

            while(res.next()){
                JTable l = new JTable();
                String datos[][]={};
                DefaultTableModel modelo;
                modelo  =  new DefaultTableModel(datos,columNames);
                tabla.setModel(modelo);
                TableColumn columna = tabla.getColumn("N°");        
                columna.setPreferredWidth(50);
                columna.setMinWidth(30);
                columna.setMaxWidth(40);
                int i=0;
                Object nuevo[]={
                    "00"+Integer.toString(i+1),
                    res.getString("codigo"),
                    res.getString("nombre"),
                    res.getString("descripcion"),
                    res.getString("medida"),
                    res.getString("marca"),
                    res.getString("Cantidad")
                };
                cantidad    = Integer.parseInt(res.getString("cantidad"));
                cant        = cantidad + Integer.parseInt(txtCantidad.getText());                 
                m.updateMaterial(txtCodigo.getText(),Integer.toString(cant));                
                txtCantidad.setText("");
                txtCodigo.setText("");
                updateTabla();
            }

        }catch(SQLException e){
            System.out.println(e);
        }
        }else{
                   
        if (fila > -1){
            String codigo  = String.valueOf(tabla.getValueAt(fila, 1));       
            cantidad = Integer.parseInt(String.valueOf(tabla.getValueAt(fila, 6)));
            cant        = cantidad + Integer.parseInt(txtCantidad.getText());                             
            m.updateMaterial(txtCodigo.getText(),Integer.toString(cant));                
            txtCantidad.setText("");
            txtCodigo.setText("");            
            updateTabla();
            fila=-1;
        }                  
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void tbnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbnBuscarActionPerformed
        conectate  con = new conectate();
        int cantidad;
        
        if(!txtBuscar.getText().equals(""))
        {
        
        String[] columNames = {"N°","Codigo","Nombre","Descripcion","Medida","Marca","cantidad"};

        try {
            //select id, nombre, apellido, codigo, ciclo from alumno where nombre like '%zand%'
            PreparedStatement pstm = con.getConnection().prepareStatement("SELECT codigo, nombre, descripcion, medida, marca, cantidad FROM material where nombre like '%"+txtBuscar.getText()+"%'");
            pstm.execute();
            ResultSet res = pstm.executeQuery();
                
                JTable l = new JTable();
                String datos[][]={};
                DefaultTableModel modelo;
                modelo  =  new DefaultTableModel(datos,columNames);
                tabla.setModel(modelo);
                TableColumn columna = tabla.getColumn("N°");        
                columna.setPreferredWidth(50);
                columna.setMinWidth(30);
                columna.setMaxWidth(40);
                int i=1;
            while(res.next()){
                  Object nuevo[]={
                    "00"+Integer.toString(i),
                    res.getString("codigo"),
                    res.getString("nombre"),
                    res.getString("descripcion"),
                    res.getString("medida"),
                    res.getString("marca"),
                    res.getString("cantidad")
                };
                modelo.addRow(nuevo); 
                i++;
            }

        }catch(SQLException e){
            System.out.println(e);
        }
        }else{
            updateTabla();
        }
    }//GEN-LAST:event_tbnBuscarActionPerformed

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked
        
        habilitar();
        fila = tabla.rowAtPoint(evt.getPoint());
        if (fila > -1){         
            txtCodigo.setText(String.valueOf(tabla.getValueAt(fila, 1)));
        }
    }//GEN-LAST:event_tablaMouseClicked

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        deshabilitar();
        updateTabla();
    }//GEN-LAST:event_formInternalFrameOpened

    private void txtRetirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRetirarActionPerformed
        conectate  con = new conectate();
        int cantidad;
        int cant;
        if(!txtBuscar.getText().equals(""))
        {
        String[] columNames = {"N°","Codigo","Nombre","Descripcion","Medida","Marca, Cantidad"};

        try {
            //select id, nombre, apellido, codigo, ciclo from alumno where nombre like '%zand%'
            PreparedStatement pstm = con.getConnection().prepareStatement("SELECT codigo, nombre, descripcion, medida, marca, cantidad FROM material where codigo like '%"+txtBuscar.getText()+"%'");
            //SELECT codigo, nombre, descripcion, medida, marca, cantidad FROM material where codigo like '%"+txtCodigo.getText()+"%'
            pstm.execute();
            ResultSet res = pstm.executeQuery();

            while(res.next()){
                JTable l = new JTable();
                String datos[][]={};
                DefaultTableModel modelo;
                modelo  =  new DefaultTableModel(datos,columNames);
                tabla.setModel(modelo);
                TableColumn columna = tabla.getColumn("N°");        
                columna.setPreferredWidth(50);
                columna.setMinWidth(30);
                columna.setMaxWidth(40);
                int i=0;
                Object nuevo[]={
                    "00"+Integer.toString(i+1),
                    res.getString("codigo"),
                    res.getString("nombre"),
                    res.getString("descripcion"),
                    res.getString("medida"),
                    res.getString("marca"),
                    res.getString("Cantidad")
                };
                cantidad    = Integer.parseInt(res.getString("cantidad"));
                cant        = cantidad - Integer.parseInt(txtCantidad.getText());                 
                m.updateMaterial(txtCodigo.getText(),Integer.toString(cant));                
                txtCantidad.setText("");
                txtCodigo.setText("");
                updateTabla();
            }

        }catch(SQLException e){
            System.out.println(e);
        }
        }else{
                   
        if (fila > -1){
            String codigo  = String.valueOf(tabla.getValueAt(fila, 1));       
            cantidad = Integer.parseInt(String.valueOf(tabla.getValueAt(fila, 6)));
            cant        = cantidad - Integer.parseInt(txtCantidad.getText());                             
            m.updateMaterial(txtCodigo.getText(),Integer.toString(cant));                
            txtCantidad.setText("");
            txtCodigo.setText("");            
            updateTabla();
            fila=-1;
        }                  
        }
    }//GEN-LAST:event_txtRetirarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        RMateriales r =  new RMateriales();
        try {

            r.mostrarReporte();

        } catch (Exception ex) {
            Logger.getLogger(Personales.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed
private void updateTabla(){             
        String[] columNames = {"N°","Codigo","Nombre","Descripcion","Medida","Marca","Cantidad"};  
        // se utiliza la funcion
        
        dtPer = m.getDatos();               
        // se colocan los datos en la tabla
        DefaultTableModel datos = new DefaultTableModel(dtPer,columNames);                        
        tabla.setModel(datos); 
        TableColumn columna = tabla.getColumn("N°");        
        columna.setPreferredWidth(50);
        columna.setMinWidth(30);
        columna.setMaxWidth(40);
    }
    ReposicionMa m= new ReposicionMa();     
    Object[][] dtPer; 
    int fila = -1;


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabla;
    private javax.swing.JButton tbnBuscar;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JButton txtRetirar;
    // End of variables declaration//GEN-END:variables
}
