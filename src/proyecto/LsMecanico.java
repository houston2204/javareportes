
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author max
 */
public class LsMecanico {
    conectate  con;
    
    public LsMecanico (){
    con = new conectate();
  }
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM alumno where ciclo like '%Mecanico%'");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][6];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT  nombre, apellido, codigo, ciclo, curso FROM alumno where ciclo like '%Mecanico%' ORDER BY nombre");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String num = "00"+Integer.toString(i+1);
            String estNombre = res.getString("nombre");
            String estApellido = res.getString("apellido");
            String estCodigo = res.getString("codigo");
            String estCiclo = res.getString("ciclo");
            String estCurso = res.getString("curso");
            
            data[i][0] = num;
            data[i][1] = estNombre;            
            data[i][2] = estApellido;            
            data[i][3] = estCodigo;            
            data[i][4] = estCiclo;         
            data[i][5] = estCurso;
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }    
}


