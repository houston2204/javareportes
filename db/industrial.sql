-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-12-2015 a las 20:35:31
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `industrial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
`id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(250) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `ciclo` varchar(20) NOT NULL,
  `curso` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `nombre`, `apellido`, `codigo`, `ciclo`, `curso`) VALUES
(4, 'Carmen', 'Moltalvo Sanchez', '2010110178', 'VII', 'Manufactura'),
(5, 'Excel', 'Ramirez martel', '2010110220', 'V', 'Manufactura'),
(6, 'Jessica', 'Ramirez Martel', '2010110342', 'IV', 'Estadística '),
(10, 'Yesica', 'Ramirez Martel', '2012110196', 'VI', 'Manufactura'),
(11, 'Nils', 'Ramirez orejon', '2010110341', 'IV', 'Estadística '),
(12, 'yemerzon', 'martin cercedo', '2013110306', 'VI', 'Manufactura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
`id` int(11) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`id`, `codigo`, `nombre`, `descripcion`) VALUES
(1, 'FOOOS2', 'Manufactura II', 'Este curso es para investigar operaciones'),
(3, 'MANF2', 'Estadística ', 'Curso de manufacturas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `herramienta`
--

CREATE TABLE IF NOT EXISTS `herramienta` (
`id` int(11) NOT NULL,
  `codpat` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `marca` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `herramienta`
--

INSERT INTO `herramienta` (`id`, `codpat`, `nombre`, `marca`, `descripcion`) VALUES
(1, 'FIIS2015001', 'Martillo', 'Stanooo', 'Matillo Stanles'),
(11, 'FIIS2015002', 'Alicate', 'Stanooo', 'Matillo Stanles de alumnio'),
(17, 'FIIS2015000', 'Martillo', 'Stanooo', 'Matillo Stanles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maquinaria`
--

CREATE TABLE IF NOT EXISTS `maquinaria` (
`id` int(11) NOT NULL,
  `codpat` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `marca` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estado` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `maquinaria`
--

INSERT INTO `maquinaria` (`id`, `codpat`, `nombre`, `marca`, `descripcion`, `estado`) VALUES
(1, 'FII20141', 'Cocina', 'Sisco', 'Cocina industrial  para restaurante', 'Regular'),
(5, 'FII2014C1', 'Cotadora', 'Sisco', 'Cortadora de madera industrial', 'Regular'),
(6, 'FII2015001', 'Computadora', 'advance', 'Computadora para el control de los  bines', 'Malo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE IF NOT EXISTS `marca` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id`, `nombre`) VALUES
(1, 'bosh'),
(2, 'gloria'),
(4, 'Stanles'),
(5, 'ideal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE IF NOT EXISTS `material` (
`id` int(11) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `medida` varchar(250) NOT NULL,
  `marca` varchar(250) NOT NULL,
  `cantidad` int(10) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id`, `codigo`, `nombre`, `descripcion`, `medida`, `marca`, `cantidad`) VALUES
(14, 'FII2015CC2', 'MARTILLO', 'MARTILLO PARA  ROMPER LADRILLOS', 'Kilometros', 'Stanles', 6),
(15, 'FII2016MAR', 'ALICATES', 'ALICATES PARA CORTAR ALAMBRESS', 'unidades', 'Stanles', 2),
(16, '23444', 'LECHE', 'bla', 'Centimetros', 'gloria', 0),
(17, 'T2015001', 'tornillo', 'tornillo para madera de 16'' ', 'unidad', 'bosh', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo`
--

CREATE TABLE IF NOT EXISTS `modelo` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelo`
--

INSERT INTO `modelo` (`id`, `nombre`) VALUES
(13, 'abireto'),
(10, 'alicate');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE IF NOT EXISTS `proyecto` (
`id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `curso` varchar(250) NOT NULL,
  `alumno` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad`
--

CREATE TABLE IF NOT EXISTS `unidad` (
`id` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad`
--

INSERT INTO `unidad` (`id`, `nombre`) VALUES
(8, 'Centimetros'),
(4, 'Gramos'),
(3, 'Kilometros'),
(1, 'kilos'),
(9, 'libras'),
(6, 'litros'),
(2, 'metros'),
(10, 'unidad'),
(7, 'unidades');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `herramienta`
--
ALTER TABLE `herramienta`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `codpat` (`codpat`);

--
-- Indices de la tabla `maquinaria`
--
ALTER TABLE `maquinaria`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `codpat` (`codpat`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `modelo`
--
ALTER TABLE `modelo`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `alumno` (`alumno`);

--
-- Indices de la tabla `unidad`
--
ALTER TABLE `unidad`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nombre` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `herramienta`
--
ALTER TABLE `herramienta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `maquinaria`
--
ALTER TABLE `maquinaria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `modelo`
--
ALTER TABLE `modelo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `unidad`
--
ALTER TABLE `unidad`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
