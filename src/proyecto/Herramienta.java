
package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Herramienta {
     conectate  con;
     public Herramienta()
     {
         con = new conectate();
     }
       /*Añade un nuevo registro*/
   public void NuevaHerramienta(String codpat, String nombre, String marca, String descripcion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "herramienta(codpat, nombre, marca, descripcion)" +
                    " VALUES(?,?,?,?)");            
            pstm.setString(1, codpat);
            pstm.setString(2, nombre);
            pstm.setString(3, marca);
            pstm.setString(4, descripcion);                                                           
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         //System.out.println(e);
           JOptionPane.showMessageDialog(null, "Esta  Herramienta ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
    public void updateherramienta(String id, String codpat, String nombre, String marca, String descripcion){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update herramienta " +
            "set codpat = ? ," +
            "nombre = ? ," +
            "marca = ? ," +                    
            "descripcion = ? " +                    
            "where codpat = ? ");            
            pstm.setString(1, codpat);                   
            pstm.setString(2, nombre);
            pstm.setString(3, marca);
            pstm.setString(4, descripcion);
            pstm.setString(5, String.valueOf(codpat));
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
 
   public void deleteHerramienta(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from herramienta where codpat = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM herramienta ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][5];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codpat, nombre, marca, descripcion " +
            " FROM herramienta" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+(i+1);
            String estCodpat = res.getString("codpat");
            String estNombre = res.getString("nombre");
            String estMarca = res.getString("marca");
            String estdescripcion = res.getString("descripcion");
           
            
            data[i][0] = estid;            
            data[i][1] = estCodpat;            
            data[i][2] = estNombre;    
            data[i][3] = estMarca;
            data[i][4] = estdescripcion;                                
            i++;
         }
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }  
}
