package proyecto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author max
 */
public class Material {
conectate  con;
public Material()
{
    con =   new   conectate();
}
/*Añade un nuevo registro*/
public void NuevoMaterial(String codigo, String nombre, String descripcion, String medida, String marca){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("INSERT INTO " + 
                    "material(codigo, nombre, descripcion, medida, marca)" +
                    " VALUES(?,?,?,?,?)");            
            pstm.setString(1, codigo);
            pstm.setString(2, nombre);
            pstm.setString(3, descripcion);
            pstm.setString(4, medida);                                                           
            pstm.setString(5, marca);
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
           JOptionPane.showMessageDialog(null, "El  del Material Ya existe");
      }
   }
    //metodo para  actualizar datos  de una herramienta
    public void updateMaterial(String codigo, String nombre, String descripcion, String medida, String marca){
       try {            
            PreparedStatement pstm = con.getConnection().prepareStatement("update material " +
            "set codigo = ? ," +
            "nombre = ? ," +
            "descripcion = ? ," +                    
            "medida = ? ,"+ 
            "marca= ? "+
            "where codigo = ? ");            
            pstm.setString(1, codigo);                   
            pstm.setString(2, nombre);
            pstm.setString(3, descripcion);
            pstm.setString(4, medida);
            pstm.setString(5, marca);
            pstm.setString(6, String.valueOf(codigo));
            pstm.execute();
            pstm.close();            
         }catch(SQLException e){
         System.out.println(e);
      }
   }
 
   public void deleteMaterial(String cod){  
            try {                
                PreparedStatement pstm = con.getConnection().prepareStatement("delete from material where codigo = ?");            
                pstm.setString(1, cod);                   
                pstm.execute();
                pstm.close();            
            }catch(SQLException e){
            System.out.println(e);
            }            
   }
    
 /*obtenemos todos los datos de la tabla*/
 public Object [][] getDatos(){
     int suma=0;
      int registros = 0;
      //obtenemos la cantidad de registros existentes en la tabla
      try{         
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT count(1) as total FROM material ");
         ResultSet res = pstm.executeQuery();
         res.next();
         registros = res.getInt("total");
         res.close();
      }catch(SQLException e){
         System.out.println(e);
      }
      
    Object[][] data = new String[registros][6];  
    //realizamos la consulta sql y llenamos los datos en "Object"
      try{    
         PreparedStatement pstm = con.getConnection().prepareStatement("SELECT " +
            "codigo, nombre, descripcion, medida, marca" +
            " FROM material" +
            " ORDER BY nombre ");
         ResultSet res = pstm.executeQuery();
         int i = 0;
         while(res.next()){
            String estid =   "00"+Integer.toString(i+1);
            String estCodigo = res.getString("codigo");
            String estNombre = res.getString("nombre");
            //suma =  suma + Integer.parseInt(estCantidad);
            String estDescripcion = res.getString("descripcion");
            String estMedida = res.getString("medida");
            String estMarca = res.getString("marca");
           
            
            data[i][0] = estid;
            data[i][1] = estCodigo;            
            data[i][2] = estNombre;    
            data[i][3] = estDescripcion;
            data[i][4] = estMedida;                                
            data[i][5] = estMarca;
            i++;
         }
         
         res.close();
          }catch(SQLException e){
         System.out.println(e);
    }
    return data;
 }    

}
